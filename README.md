| Command                                     | Description                                       |
|---------------------------------------------|---------------------------------------------------|
| `git init`                                 | Initialize a local Git repository                |
| `git clone repo_url`                       | Clone a public repository                        |
| `git clone ssh://git@github.com/[username]/[repository-name].git` | Clone a private repository |
| `git status`                               | Check the status of the working directory       |
| `git add [file-name]`                      | Add a specific file to the staging area          |
| `git add -A`                               | Add all new and changed files to the staging area|
| `git commit -m "[commit message]"`         | Commit changes with a descriptive message       |
| `git rm -r [file-name.txt]`                | Remove a file or folder from the repository      |
| `git branch`                               | List all branches (the asterisk denotes the current branch) |
| `git branch -a`                            | List all branches, both local and remote        |
| `git branch [branch name]`                 | Create a new local branch                        |
| `git branch -d [branch name]`              | Delete a local branch                            |
| `git branch -D [branch name]`              | Delete a local branch forcefully                 |
| `git push origin --delete [branch name]`   | Delete a remote branch on the remote repository  |
| `git checkout -b [branch name]`            | Create a new branch and switch to it             |
| `git checkout -b [branch name] origin/[branch name]` | Clone and switch to a remote branch |
| `git branch -m [old branch name] [new branch name]` | Rename a local branch                |
| `git checkout [branch name]`               | Switch to an existing branch                     |
| `git checkout -- [file-name.txt]`          | Discard changes made to a specific file          |
| `git merge [branch name]`                  | Merge a branch into the currently active branch |
| `git merge [source branch] [target branch]` | Merge a branch into a specified target branch   |
| `git stash`                                | Stash changes in a dirty working directory      |
| `git stash clear`                          | Remove all stashed entries                       |
| `git push origin [branch name]`            | Push a local branch to the remote repository    |
| `git push -u origin [branch name]`         | Push changes to the remote repository and set it as the default |
| `git push`                                 | Push changes to the remote repository (for the remembered branch) |
| `git push origin --delete [branch name]`   | Delete a remote branch on the remote repository  |
| `git pull`                                 | Update the local repository to the newest commit |
| `git pull origin [branch name]`            | Pull changes from a remote repository            |
| `git remote add origin ssh://git@github.com/[username]/[repository-name].git` | Add a remote repository |
| `git remote set-url origin ssh://git@github.com/[username]/[repository-name].git` | Set the repository's origin URL to SSH |
| `git log`                                  | View the commit history and changes             |
| `git log --summary`                        | View commit history with detailed information   |
| `git log --oneline`                        | View a concise commit history                   |
| `git diff [source branch] [target branch]` | Preview changes between two branches            |
| `git revert commitid`                      | Create a new commit that undoes changes from a specific commit |
| `git config --global user.name "your_username"` | Set your global Git username            |
| `git config --global user.email "your_email_address@example.com"` | Set your global Git email address |
| `git config --global --list`               | Get a list of global Git configurations          |
